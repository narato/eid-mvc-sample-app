﻿using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using EidMVCSample.Code;
using Sun.Identity.Saml2;
using Sun.Identity.Saml2.Exceptions;

namespace EidMVCSample.Controllers
{
    public class AuthenticationController : Controller
    {
        /// <summary>
        /// Gaat een request naar Fedict sturen om in te loggen. 
        /// We worden dus automatisch geredirect naar een webpagina van Fedict.
        /// </summary>
        public void Login()
        {
            ServiceProviderUtility serviceProviderUtility = HttpContext.Cache["spu"] as ServiceProviderUtility;

            if (serviceProviderUtility == null)
            {
                serviceProviderUtility = new ServiceProviderUtility(System.Web.HttpContext.Current);
                HttpContext.Cache["spu"] = serviceProviderUtility;
            }

            string idpEntityId = null;
            IEnumerator idps = serviceProviderUtility.IdentityProviders.Keys.GetEnumerator();

            if (idps.MoveNext())
                idpEntityId = (string)idps.Current;

            // Store parameters for initializing SSO
            NameValueCollection parameters = Saml2Utils.GetRequestParameters(System.Web.HttpContext.Current.Request);

            //Binding is HttpPostProtocolBinding. Dit kan ook HttpArtifactProtocolBinding zijn.
            parameters[Saml2Constants.Binding] = Saml2Constants.HttpPostProtocolBinding;
            parameters[Saml2Constants.AllowCreate] = "true";
            parameters[Saml2Constants.AuthnContextClassRef] = ConfigurationManager.AppSettings[Constants.AuthenticationContextClassReff];
            parameters[Saml2Constants.IdpEntityId] = ConfigurationManager.AppSettings[Constants.IdpEntityId];

            try
            {
                // Check for required parameters...
                if (idpEntityId == null)
                    throw new ServiceProviderUtilityException("IDP Entity ID not specified nor discovered.");

                // Perform SP initiated SSO
                serviceProviderUtility.SendAuthnRequest(System.Web.HttpContext.Current, idpEntityId, parameters);
            }
            catch (Saml2Exception se)
            {
                //Log/Handle
            }
            catch (ServiceProviderUtilityException spue)
            {
                //Log/Handle
            }
        }

        /// <summary>
        /// Deze methode wordt aangeroepen door de client wanneer er op Afmelden wordt gedrukt en stuurt een request naar Fedict om uit te loggen. 
        /// Ook hier komen we, zeer kort, op een webpagina van Fedict terecht.
        /// </summary>
        [Authorize]
        public void LogOff()
        {
            ServiceProviderUtility serviceProviderUtility = HttpContext.Cache["spu"] as ServiceProviderUtility;
            if (serviceProviderUtility == null)
            {
                serviceProviderUtility = new ServiceProviderUtility(System.Web.HttpContext.Current);
                HttpContext.Cache["spu"] = serviceProviderUtility;
            }

            
            string idpEntityId = ConfigurationManager.AppSettings[Constants.IdpEntityId];

            // Store parameters for initializing SLO
            NameValueCollection parameters = new NameValueCollection();
            parameters[Saml2Constants.AllowCreate] = "true";
            parameters[Saml2Constants.AuthnContextClassRef] = ConfigurationManager.AppSettings[Constants.AuthenticationContextClassReff];
            parameters[Saml2Constants.SessionIndex] = Session[Constants.SessionIndex]?.ToString() ?? string.Empty;
            parameters[Saml2Constants.SubjectNameId] = Session[Constants.SubjectNameId]?.ToString() ?? string.Empty;
            parameters[Saml2Constants.Binding] = Saml2Constants.HttpPostProtocolBinding;
            parameters[Saml2Constants.RelayState] = ConfigurationManager.AppSettings[Constants.LogoutRedirectUrl];

            try
            {
                // Check for required parameters...
                if (string.IsNullOrEmpty(idpEntityId))
                    throw new ServiceProviderUtilityException("IDP Entity ID not specified.");

                if (string.IsNullOrEmpty(parameters[Saml2Constants.SubjectNameId]))
                    throw new ServiceProviderUtilityException("SubjectNameId not specified.");

                if (string.IsNullOrEmpty(parameters[Saml2Constants.SessionIndex]))
                    throw new ServiceProviderUtilityException("SessionIndex not specified.");

                // Perform SP initiated SSO
                serviceProviderUtility.SendLogoutRequest(System.Web.HttpContext.Current, idpEntityId, parameters);

                // If SOAP was the binding and no exception thrown, redirect to the relay state
                if (parameters[Saml2Constants.Binding] == Saml2Constants.HttpSoapProtocolBinding)
                {
                    string relayState = parameters[Saml2Constants.RelayState];
                    Saml2Utils.ValidateRelayState(relayState, serviceProviderUtility.ServiceProvider.RelayStateUrlList);
                    Response.Redirect(relayState);
                }
            }
            catch (Saml2Exception se)
            {
                //Log/Handle
            }
            catch (ServiceProviderUtilityException spue)
            {
                //Log/Handle
            }
        }

        /// <summary>
        /// Nadat we zijn ingelogd op de Fedict pagina, worden we teruggestuurd naar deze methode waar we de response
        /// gaan nakijken op eventuele fouten. Als de login successvol is dan kunnen we de opgevraagde gegevens uit de response halen.
        /// </summary>
        /// <returns></returns>
        public ActionResult SingleSignIn()
        {
            try
            {
                var serviceProviderUtility = HttpContext.Cache["spu"] as ServiceProviderUtility;

                if (serviceProviderUtility == null)
                {
                    serviceProviderUtility = new ServiceProviderUtility(System.Web.HttpContext.Current);
                    HttpContext.Cache["spu"] = serviceProviderUtility;
                }

                var authnResponse = serviceProviderUtility.GetAuthnResponse(System.Web.HttpContext.Current);

                string rijksregister = GetAttributeValue(authnResponse, Constants.RijksregisterKey);
                string voornaam = GetAttributeValue(authnResponse, Constants.NaamKey);
                string familienaam = GetAttributeValue(authnResponse, Constants.FamilienaamKey);

                Session[Constants.SessionRijksregisternummer] = rijksregister;
                Session[Constants.LoggedInUser] = voornaam + " " + familienaam;
                Session[Constants.SessionIndex] = authnResponse.SessionIndex;
                Session[Constants.SubjectNameId] = authnResponse.SubjectNameId;

                FormsAuthentication.SetAuthCookie(voornaam + " " + familienaam, false);

                //Als er een return url in de sessie zit dan gaan we hier naartoe navigeren.
                if (Session[Constants.ReturnUrl] != null)
                {
                    var url = Session[Constants.ReturnUrl].ToString();
                    Session[Constants.ReturnUrl] = null;
                    return Redirect("../" + url);
                }
            }
            catch (Saml2Exception se)
            {
                //Log/Handle
            }
            catch (ServiceProviderUtilityException spue)
            {
                //Log/Handle
            }

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Hier komen we de applicatie terug binnen nadat Fedict ons heeft uitgelogd.
        /// </summary>
        /// <returns></returns>
        public ActionResult SingleSignOut()
        {
            ServiceProviderUtility serviceProviderUtility = HttpContext.Cache["spu"] as ServiceProviderUtility;
            if (serviceProviderUtility == null)
            {
                serviceProviderUtility = new ServiceProviderUtility(System.Web.HttpContext.Current);
                HttpContext.Cache["spu"] = serviceProviderUtility;
            }

            NameValueCollection parameters = Saml2Utils.GetRequestParameters(System.Web.HttpContext.Current.Request);
            string samlResponse = parameters[Saml2Constants.ResponseParameter];

            try
            {
                // Perform action based on what was received...
                if (!string.IsNullOrEmpty(samlResponse))
                {
                    // do local app specific post-logout behavior
                    FormsAuthentication.SignOut();
                    
                    Session[Constants.SessionIndex] = null;
                    Session[Constants.SubjectNameId] = null;
                    Session[Constants.SessionPostCodeBurger] = null;
                    Session[Constants.SessionRijksregisternummer] = null;
                    Session[Constants.LoggedInUser] = null;

                    // redirect to either the relay state or the fedlet's default url
                    if (!string.IsNullOrEmpty(parameters[Saml2Constants.RelayState]))
                    {
                        string redirectUrl = parameters[Saml2Constants.RelayState];
                        Saml2Utils.ValidateRelayState(redirectUrl, serviceProviderUtility.ServiceProvider.RelayStateUrlList);

                        return Redirect(redirectUrl);
                    }

                    return Redirect(ConfigurationManager.AppSettings[Constants.LogoutRedirectUrl]);
                }
            }
            catch (Saml2Exception se)
            {
                //Log/Handle
            }
            catch (ServiceProviderUtilityException spue)
            {
                //Log/Handle
            }

            return RedirectToAction("Index", "Home");
        }

        #region Helpers
        private string GetAttributeValue(AuthnResponse authnResponse, string key)
        {
            ArrayList values = (ArrayList)authnResponse.Attributes[key];

            return values.Cast<string>().Aggregate(string.Empty, (current, value) => current + value);
        }

        #endregion
    }
}