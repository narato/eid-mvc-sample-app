﻿namespace EidMVCSample.Code
{
    public static class Constants
    {
        public const string SessionPostCodeBurger = "SessionPostCodeBurger";
        public const string SessionRijksregisternummer = "SessionRijksregisternummer";
        public const string LoggedInUser = "LoggedInUser";
        public const string SessionIndex = "SessionIndex";
        public const string SubjectNameId = "SubjectNameId";
        public const string FamilienaamKey = "FamilienaamKey";
        public const string NaamKey = "NaamKey";
        public const string RijksregisterKey = "RijksregisterKey";
        public const string ReturnUrl = "ReturnUrl";

        //Web config values
        public const string IdpEntityId = "IdpEntityId";
        public const string AuthenticationContextClassReff = "AuthenticationContextClassReff";
        public const string LogoutRedirectUrl = "LogoutRedirectUrl";
    }
}